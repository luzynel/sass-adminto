$(document).ready(function() {
    var fh = $(".footer").height();
    $("body").css({
        'padding-bottom' : + fh
    });

    var wh = $(".consultation-wrapper").width();
    $(".consultation-box.position-fixed").css({
        width: + wh
    });

    var ww = $(window).width();
    if (ww >= 768) {
        $("body.modalFrame .modal-backdrop").hide();
        $("body.modalFrame").removeClass('modal-open');
    }else {
        $("body.modalFrame .modal-backdrop").show();
    }

});

$(window).resize(function() {
    var fh = $(".footer").height();
    $("body").css({
        'padding-bottom' : + fh
    });

    var wh = $(".consultation-wrapper").width();
    $(".consultation-box.position-fixed").css({
        width: + wh
    });

    var ww = $(window).width();
    if (ww >= 768) {
        $("body.modalFrame .modal-backdrop").hide();
        $("body.modalFrame").removeClass('modal-open');
    }else {
        $("body.modalFrame .modal-backdrop").show();
    }
});

// $(window).scroll(function() {
//     var wh = $(".consultation-wrapper").width();
//     if ($(this).scrollTop()) {
//         $(".consultation-box").css({
//             position:"fixed",
//             width: + wh
//         });
//     } else {
//         $(".consultation-box").css({
//             position:"initial",
//             width:"auto"
//         });
//     }
// });

$('.top-trainer-banner').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fe-chevron-left' aria-hidden='true'></i></button>",
    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fe-chevron-right' aria-hidden='true'></i></button>",
    responsive: [
        {
          breakpoint: 1100,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            infinite: true
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
    ]
});

$('.head-trainer-banner').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fe-chevron-left' aria-hidden='true'></i></button>",
    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fe-chevron-right' aria-hidden='true'></i></button>",
    responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
    ]
});

$('.trainer-images').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false
          }
        }
    ]
});

$('.trainer-collapse').click(function () {
    $('.toggle-content  .trainer-images').slick('refresh');
   // $('.collapse .trainer-images').slick("slickSetOption", "draggable", true, true); 
});

$('.toggle-wrap .trainer-collapse').click(function () {
  $(this).toggleClass('collapsed');
  //$(this).parents('.trainer-tbl-head').siblings('.toggle-content').toggleClass('show');  
  $(this).parents('.trainer-tbl-head').siblings('.toggle-content').slideToggle( "slow" );
  $('.toggle-content .trainer-images').slick('refresh');
});



$('#FormBox .consult-btn').click(function() {
    $(this).parents('.card-body').hide();
    $(this).parents('.card-body').siblings('#ConfirmBox').show();
});
$('#ConfirmBox .form-group .consult-btn.consult-next').click(function() {
    $(this).parents('.card-body').hide();
    $(this).parents('.card-body').siblings('#ThankBox').show();
});
$('.card-body .consult-btn.consult-back').click(function() {
    $(this).parents('.card-body').hide();
    $(this).parents('.card-body').siblings('#FormBox').show();
    $(this).parents('.card-body').siblings('#FormBox2').show();
});

$('#FormBox2 .consult-btn.consult-log').click(function() {
    $(this).parents('.card-body').hide();
    $(this).parents('.card-body').siblings('#LogBox').show();
});
$('#LogBox .consult-btn.consult-confirm').click(function() {
    $(this).parents('.card-body').hide();
    $(this).parents('.card-body').siblings('#ConfirmBox').show();
});